// Setup the dependencies
const express = require("express") 
const mongoose = require("mongoose");



//
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Database connection
mongoose.connect("mongodb+srv://derickjayyy12345:admin123@zuitt-bootcamp.mx8pwpr.mongodb.net/s35?retryWrites=true&w=majority",
	{
		
		useNewUrlparser: true,
		useUnifiedTopology: true
	}	

);

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database`));


// Add task route
//Allows all the task routes created in the 'taskRoute.js' files to use "/tasks" 
app.use("/tasks", taskRoute)

app.listen( port, () => console.log(`Now listening to port ${port}`));




