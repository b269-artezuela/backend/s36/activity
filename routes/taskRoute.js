// Defines WHEN particular constrollers will be used
//  Contain all the endpoints and responses that we can get from controllers





const express = require("express")
//Creates a router instance that functions as a middleware and routing system
const router = express.Router();

const taskController = require("../controllers/taskController")

// Route to get all the tasks
// http : //localhost:3001/tasks
router.get("/", (req,res) => {

	taskController.getAllTasks().then(resultFromController => 
		res.send(resultFromController)); 
})


// Route to create all the tasks
router.post("/",(req,res) => {
	taskController.createTask(req.body).then(
		resultFromController => res.send(resultFromController));
})

// Route to delete task

router.delete("/:id", (req,res) => {
	taskController.deleteTask(req.params.id).then(
		resultFromController => res.send(resultFromController));
})

/// Activity/////////
router.get("/:id", (req,res) => {
	taskController.getSpecificTasks(req.params.id).then(
		resultFromController => res.send(resultFromController));
})


router.patch("/:id/complete", (req,res) => {
	taskController.completeTask( req.params.id).then(resultFromController => res.send(resultFromController));
});



module.exports = router